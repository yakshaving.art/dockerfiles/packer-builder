# This is a multistage build.

# Helper image for installing packer and plugins
# hadolint ignore=DL3007
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master AS installer

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

# hadolint ignore=DL3018
RUN apk -qq --no-cache add \
		gnupg \
		gzip \
	# install binaries from hoarder, multiarch
	&& bash /usr/local/bin/unhoard.sh goss "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh shellcheck "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh packer "${TARGETOS}" "${TARGETARCH}" \
	# install other things
	&& echo "done"

# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master
LABEL maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use as a builder image for packer images"

# Disable packer's call home by default
ENV CHECKPOINT_DISABLE="1"

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

ENV PATH="$PATH:/usr/local/gcloud/google-cloud-sdk/bin"

COPY --from=installer --chown=root:root \
	/usr/local/bin/packer* \
	/usr/local/bin/goss \
	/usr/local/bin/shellcheck \
	/usr/local/bin/

# hadolint ignore=DL3017,DL3018
RUN \
	# We bind-mount scripts and tests so that we don't pollute the end image
	# NOTE: instead of mounting separate dirs, we mount both so that we don't have
	# empty /mnt/scripts and /mnt/tests directory leftovers in the image :ocd:
	--mount=type=bind,source=.gitlab.d/ci,target=/mnt,readonly \
	# We also bind-mount goss and trivy binaries from the docker-builder image
	--mount=type=bind,source=goss,target=/bin/goss,readonly \
	--mount=type=bind,source=trivy,target=/bin/trivy,readonly \
	# And we also bind-mount .trivyignore file
	--mount=type=bind,source=.trivyignore,target=/.trivyignore,readonly \
	# Main flow below
	apk -qq --no-cache upgrade \
	# install gcloud: TODO: shasums
	&& apk -qq --no-cache add \
		py3-crcmod \
		python3 \
	&& mkdir -p "/usr/local/gcloud" \
	&& wget -qqO - "https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz" \
		| tar -C "/usr/local/gcloud" -zxf - \
	&& /usr/local/gcloud/google-cloud-sdk/install.sh --quiet --override-components alpha beta core gsutil \
	&& gcloud config set component_manager/disable_update_check true \
	&& gcloud config set core/disable_prompts true \
	&& gcloud config set core/disable_usage_reporting true \
	&& gcloud config set core/show_structured_logs always \
	&& gcloud config set survey/disable_prompts true \
	# cleanup logs and backups
	&& rm -rf "${HOME}/.config/gcloud/logs" \
	&& rm -rf "/usr/local/gcloud/google-cloud-sdk/.install/.backup/" \
	# fix permissions
	&& ( set +f; chown root:root /usr/local/bin/* \
		&& chmod 0755 /usr/local/bin/* ) \
	# at this point, the _build_ is done, and we proceed to run _tests_ from within the image,
	# in order to abort the whole thing if they fail, and never push anything unsafe to the registry
	# these stages should be the last, and they should also be self-contained, i.e. do not install
	# any dependencies to not pollute the end image
	&& bash /mnt/scripts/skkrty_inside_build.sh \
	&& bash /mnt/scripts/tests_inside_build.sh \
	&& echo "done"
